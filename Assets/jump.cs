﻿using UnityEngine;
 using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class jump : MonoBehaviour
{
    public float Jump;
    public float jumpcount = 2;
    float moveVelocity;
    public bool Canclick = true;
    bool grounded = false;
    public float duration = 0.5f;
    public Canvas replay;
    public Canvas revive;
    public GameObject jerry;
    private void Start()
    {
        revive.enabled = false;
        replay.enabled = false;
    }
    private void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (jumpcount != 0)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, Jump);

                    jumpcount--;

                }
            }
            
        }

    }
   private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "box")
        {
            Time.timeScale = 0;
            revive.enabled = true;
            replay.enabled = true;
        }
        if (collision.gameObject.tag == "ground")
        {
            jumpcount = 2;

        }


    }
   /* void enablejump()
    {
        jumpcount =2;
        Canclick = true;

    }*/
  
    public void reload()
    {

        SceneManager.LoadScene("tap", LoadSceneMode.Single);
       
    }
    public void continu()
    {
        Time.timeScale = 1;
    }
    public void disablebut()
    {
        replay.enabled = false;
        revive.enabled = false;
    }
}
    