﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class move : MonoBehaviour {
    public int mov = 5;
    public Canvas replay;
    public int x=0;
    
	// Use this for initialization
	void Start () {
        
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector2.left * mov * Time.deltaTime);
        if(SCORE.scorevalue>=x+10)
        {
           
            mov += 5;
            x += 10;
            
        }
		
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "wall")
        {
            SCORE.scorevalue += 1;


            Destroy(this.gameObject);
        }
        
    }

  
}
